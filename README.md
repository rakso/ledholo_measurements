# Pomiary urządzeń LedHolo

#### Wyświetlacz "40"

- **Standby**
  - _Bez oborotów_
    - Bez wyświetlania: **580mA**
    - Z wyświetlaną linią: **X**
  - _Z obrotami_
    - Bez wyświetlania: **700mA**
- **Praca**
  - _Średnia animacja_
    - Szybkość "normal": od **750mA** do **850mA**
    - Szybkość "fast": **X**
- **Test z białym tłem**
  - _Biała maska (mniej biała niż zdjęcie)_
    - Szybkość "normal": **X**
    - Szybkość "fast": **X**
  - _Białe zdjęcie (jaśniejsze niż maska)_
    - Szybkość "normal": **1100mA**
    - Szybkość "fast": **X**
- **Rozkręcanie się łopat**
  - Szybkość "normal": **X**
  - Szybkość "fast": **X**

#### Wyświetlacz "50"

- **Standby**
  - _Bez oborotów_
    - Bez wyświetlania: **760mA**
    - Z wyświetlaną linią: **1100mA**
  - _Z obrotami_
    - Bez wyświetlania: **770mA**
- **Praca**
  - _Średnia animacja_
    - Szybkość "normal": od **1200mA** do **1320mA**
    - Szybkość "fast": **X**
- **Test z białym tłem**
  - _Biała maska (mniej biała niż zdjęcie)_
    - Szybkość "normal": **X**
    - Szybkość "fast": **X**
  - _Białe zdjęcie (jaśniejsze niż maska)_
    - Szybkość "normal": **2400mA**
    - Szybkość "fast": **X**
- **Rozkręcanie się łopat**
  - Szybkość "normal": **X**
  - Szybkość "fast": **X**

#### Wyświetlacz "65H"

- **Standby**
  - _Bez oborotów_
    - Bez wyświetlania: **370mA**
    - Z wyświetlaną linią: **513mA**
  - _Z obrotami_
    - Bez wyświetlania: **695mA**
- **Praca**
  - _Średnia animacja_
    - Szybkość "normal": od **760mA** do **890mA**
    - Szybkość "fast": **X**
- **Test z białym tłem**
  - _Biała maska (mniej biała niż zdjęcie)_
    - Szybkość "normal": **1460mA**
    - Szybkość "fast": **X**
  - _Białe zdjęcie (jaśniejsze niż maska)_
    - Szybkość "normal": **2390mA**
    - Szybkość "fast": **X**
- **Rozkręcanie się łopat**
  - Szybkość "normal": **1000mA**
  - Szybkość "fast": **X**

#### Wyświetlacz "65X"

- **Standby**
  - _Bez oborotów_
    - Bez wyświetlania: **457mA**
    - Z wyświetlaną linią: **546mA**
  - _Z obrotami_
    - Bez wyświetlania: **730mA**
- **Praca**
  - _Średnia animacja_
    - Szybkość "normal": od **880mA** do **940mA**
    - Szybkość "fast": **X**
- **Test z białym tłem**
  - _Biała maska (mniej biała niż zdjęcie)_
    - Szybkość "normal": **1300mA**
    - Szybkość "fast": **X**
  - _Białe zdjęcie (jaśniejsze niż maska)_
    - Szybkość "normal": **2050mA**
    - Szybkość "fast": **X**
- **Rozkręcanie się łopat**
  - Szybkość "normal": **1133mA**
  - Szybkość "fast": **X**

#### Wyświetlacz "78"

- **Standby**
  - _Bez oborotów_
    - Bez wyświetlania: **730mA**
    - Z wyświetlaną linią: **875mA**
  - _Z obrotami_
    - Bez wyświetlania: **617mA**
- **Praca**
  - _Średnia animacja_
    - Szybkość "normal": od **1100mA** do **1200mA**
    - Szybkość "fast": od **1400mA** do **1460mA**
- **Test z białym tłem**
  - _Biała maska (mniej biała niż zdjęcie)_
    - Szybkość "normal": **1680mA**
    - Szybkość "fast": **2000mA**
  - _Białe zdjęcie (jaśniejsze niż maska)_
    - Szybkość "normal": **2300mA**
    - Szybkość "fast": **2600mA**
- **Rozkręcanie się łopat**
  - Szybkość "normal": **2000mA**
  - Szybkość "fast": **2300mA** (chwilowo, potem 2100mA)

### Ogólne podsumowanie:

| Nazwa urządzenia |      pobór prądu przy animcaji       |        pobór prądu "na biało"         |
| :--------------: | :----------------------------------: | :-----------------------------------: |
|       "40"       |              **850mA**               |              **1100mA**               |
|       "50"       |              **1320mA**              |              **2400mA**               |
|      "65H"       |              **890mA**               |              **2300mA**               |
|      "65X"       |              **940mA**               |              **2005mA**               |
|       "78"       | normal/fast: **1200mA** / **1460mA** | normal/fast: **2300maA** / **2600mA** |
